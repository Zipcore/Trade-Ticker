#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Trade Ticker"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Simple ticker for trade server, where players can place ads."
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>

#define LoopClients(%1) for(int %1=1;%1<=MaxClients;++%1)

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

Handle g_aPlayers = null;
Handle g_aMsg = null;
Handle g_aTimeAdded = null;

int g_iCurrentMsg;

#define MAX_MSG_LEN 512

/* Convars */

ConVar g_cvTimeMul;
ConVar g_cvTimer;

/* Settings */
int g_iTimeMul;
float g_fTimer;

/* Timer */
Handle g_hTimer = null;

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("trade-ticker");
	return APLRes_Success;
}

public void OnPluginStart()
{
	CreateConVar("trade_ticker_version", PLUGIN_VERSION, "Trade-Ticker version", FCVAR_DONTRECORD|FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	g_cvTimeMul = CreateConVar("trade_ticker_time_mul", "15", "Hold advert for this * adverts in queue.", FCVAR_PLUGIN);
	g_iTimeMul = GetConVarInt(g_cvTimeMul);
	HookConVarChange(g_cvTimeMul, Action_OnSettingsChange);
	
	g_cvTimer = CreateConVar("trade_ticker_timer", "5.0", "Time between adverts.", FCVAR_PLUGIN);
	g_fTimer = GetConVarFloat(g_cvTimer);
	HookConVarChange(g_cvTimer, Action_OnSettingsChange);
	
	AutoExecConfig(true, "trade-ticker");
	
	RegConsoleCmd("sm_advert", Cmd_Advert);
	
	g_hTimer = CreateTimer(g_fTimer, Timer_UpdateAdverts, _, TIMER_REPEAT);
	
	g_iCurrentMsg = 0;

	g_aPlayers = CreateArray(1);
	g_aMsg = CreateArray(MAX_MSG_LEN);
	g_aTimeAdded = CreateArray(1);
}

public void Action_OnSettingsChange(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	if (cvar == g_cvTimeMul)
	{
		g_iTimeMul = StringToInt(newvalue);
	}
	else if (cvar == g_cvTimer)
	{
		g_fTimer = StringToFloat(newvalue);
		
		/* Restart timer */
		
		if(g_hTimer != null)
			CloseHandle(g_hTimer);
		
		if(g_fTimer > 0.0) 
			g_hTimer = CreateTimer(g_fTimer, Timer_UpdateAdverts, _, TIMER_REPEAT);
		else g_hTimer = null;
	}
}

public Action Cmd_Advert(int client, int args)
{
	if(client == 0)
	{
		ReplyToCommand(client, "This command is for ingame use only.");
		return Plugin_Handled;
	}
	
	if(args < 1)
	{
		ReplyToCommand(client, "Usage: sm_advert <message>.");
		return Plugin_Handled;
	}
	
	char msg[MAX_MSG_LEN];
	
	char buffer[MAX_MSG_LEN];
	for (int i = 1; i <= args; i++)
	{
		GetCmdArg(i, buffer, MAX_MSG_LEN);
		Format(msg, MAX_MSG_LEN, "%s %s", msg, buffer);
	}
	
	ReplaceString(msg, MAX_MSG_LEN, "\n", "");
	ReplaceString(msg, MAX_MSG_LEN, "\t", "");
	ReplaceString(msg, MAX_MSG_LEN, "<", "");
	ReplaceString(msg, MAX_MSG_LEN, ">", "");
	
	char auth[32];
	GetClientAuthId(client, AuthId_Steam2, auth, 32);
	
	// If player has already a msg
	for (int i = 0; i < GetArraySize(g_aPlayers); i++)
	{
		if(GetArrayCell(g_aPlayers, i) == client)
		{
			SetArrayString(g_aMsg, i, msg);
			
			int ttl = GetArraySize(g_aPlayers) * g_iTimeMul;
			SetArrayCell(g_aTimeAdded, i, GetTime()+ttl);
			
			ReplyToCommand(client, "Your message has replaced your old one and is queued for ~ %ds.", ttl);
			
			return Plugin_Handled;
		}
	}
	
	PushArrayCell(g_aPlayers, client);
	PushArrayString(g_aMsg, msg);
	
	int ttl = GetArraySize(g_aPlayers) * g_iTimeMul;
	PushArrayCell(g_aTimeAdded, GetTime()+ttl);
	
	ReplyToCommand(client, "Your message is queued for ~ %ds.", ttl);
	
	return Plugin_Handled;
}

public Action Timer_UpdateAdverts(Handle timer, any data)
{
	int time = GetTime();
	
	//Delete invalid adverts
	for (int i = 0; i < GetArraySize(g_aPlayers); i++)
	{
		PrintToChatAll("ttl=time-timeadded: %d", GetArrayCell(g_aTimeAdded, i));
		
		//Check ttl
		if(time < GetArrayCell(g_aTimeAdded, i) && IsClientInGame(GetArrayCell(g_aPlayers, i)))
			continue;
			
		RemoveFromArray(g_aPlayers, i);
		RemoveFromArray(g_aMsg, i);
		RemoveFromArray(g_aTimeAdded, i);
	}
	
	//No adverts left
	if(GetArraySize(g_aPlayers) < 1)
		return Plugin_Continue;
	
	//Ready for next advert
	g_iCurrentMsg++;
	if(g_iCurrentMsg >= GetArraySize(g_aPlayers))
		g_iCurrentMsg = 0;
	
	//Get Advert emssage
	char msg[MAX_MSG_LEN];
	GetArrayString(g_aMsg, g_iCurrentMsg, msg, MAX_MSG_LEN);
	
	//Build HUD
	char hud[512];
	Format(hud, 512, "<font size='18'>*Advert* by <font color='#CC00CC'>%N</font>\n<font color='#00DD00'>%s</font></font>", GetArrayCell(g_aPlayers, g_iCurrentMsg), msg);
	
	//Send HintText
	LoopClients(client)
	{
		if(IsClientInGame(client))
			PrintHintText(client, hud);
	}
	
	return Plugin_Continue;
}